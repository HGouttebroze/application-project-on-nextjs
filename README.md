# Hugues next.js App

## Install next.js use npm

`npx create-next-app (projectName) --use-npm`

### NPM or YARN ?

to use npm, add the flag `---npm`
            or
to use yarn, add the flag `---yarn`

## Install TypeScript

First, 

Next.js provides an integrated TypeScript experience out of the box, similar to an IDE.

To get started, create an empty tsconfig.json file in the root of your project:

`touch tsconfig.json`

Next, install typescript, @types/react, and @types/node by running:
`npm install --save-dev typescript @types/react @types/node`

TypeScript strict mode is turned off by default. When you feel comfortable with TypeScript, it's recommended to turn it on in your tsconfig.json.

All right! You can now start converting files from .js to .tsx !

### IMPORTANT!

Read intruction in NextJS official documentation: `https://nextjs.org/docs/basic-features/typescript`

## Install Tailwind

go tu url: `https://tailwindcss.com/docs/guides/nextjs`
& follow instructions

### If you're on Next.js v10

`npm install tailwindcss@latest postcss@latest autoprefixer@latest`

Don't forget to delete all CSS styles installed by default like in index.js & global.css files

### Create configuration files

Generate your tailwind.config.js and postcss.config.js files:

`npx tailwindcss init -p`

### Include Tailwind in your CSS

Open the ./styles/globals.css file that Next.js generates for you by default and use the @tailwind directive to include Tailwind's base, components, and utilities styles. 
Replacing the original file contents.

### Stop and restart build Next JS app

Restart a `npm run dev` 
You can add, change or remove a npm script in your `package.json`

YOU CAN NOW USE TAILWIND.CSS FOR YOUR STYLE

----------------------------------------------------------------------------------------------------------------------------------------

# README.md by default (to purge)
This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/import?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
