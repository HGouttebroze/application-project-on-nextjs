import { report } from "process"
import React, { Component } from "react"
import Layout from "../components/Layout"

import Link from 'next/link';
import fetch from 'isomorphic-fetch';

const HOST_NAME = process.env.HOST_NAME || 'http://jsonplaceholder.typicode.com/';

export default class extends React.Component {
    static async getInitialProps() {
        const data = await fetch(`${HOST_NAME}posts/`);
        const items = await data.json();
        return {
            items
        };
    }
    render() {
        const { items } = this.props;
        return (
            <section>
                <h1>Node.js Server Side Render in the Age of APIs</h1>
                <ul>
                    { items.map(i => <li><Link href={ `/post?id=${i.id}` }>{ i.title }</Link></li>) }
                </ul>
            </section>
        );
    }
}

// export default function Tablatures({ res }) {
//     return (
//         <Layout page={"Page " + res.name}>
//             <div className="relative hover:shadow-md p-8 border-blue-300 sm:rounded-3xl bg-blue-100 md:w-auto flex mx-5">
//                 <div className="text-center">
//                     <img 
//                         src={res.logo_url} 
//                         alt={res.name} 
//                     />
//                 </div>
//                 <h2 className="text-2xl mb-6 uppercase tracking-wider">{res.name}</h2>
//                 <p className="pt-5 text-blue-500">
//                     <a target="_blank" href={res.url}></a>
//                 </p>
//             </div>
//         </Layout>
//     );
// }

// export async function getServerSideProps({res}) {
    
//     console.log(res)
//     try {
//         const res = await fetch(
//             // 'http://www.songsterr.com/a/ra/songs.json?pattern=Cure'
//             'https://jsonplaceholder.typicode.com/posts'
//         );
//         const result = await res.json();
        
//         return {
//             props: { res: result[0]}
//         }; 
//       }  catch (err) {
//             console.error(err);
//         }
    
// }
// export default class Tablatures extends Component {

//     static async getInitialProps() {

//         const res = await fetch('http://www.songsterr.com/a/ra/songs.json?pattern=Cure/song')
//         // http://www.songsterr.com/a/wa/bestMatchForQueryString?s={song title}&a={artist name}
//         // http://www.songsterr.com/a/wa/song?id={id}
//         // http://www.songsterr.com/a/wa/artist?id={id}

//         const song = await res.json()

//         return {songs:song}
//     }
// render() {

//     return (
//         <Layout page="tablatures">
//             <h1 className="text-4xl">Tablatures</h1>
//             <br />
//             <ul>
                
                
//             </ul>
//         </Layout> 
//     )
//   }
// }
        // <Layout page="tablatures">
        //     
        //     <p>
        //      Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis vitae, dicta cum qui quidem, blanditiis 
        //      repudiandae culpa magni dignissimos nulla ullam corrupti rem amet dolorem nesciunt quam illum? Nesciunt 
        //      impedit provident ab dolorum sint porro odio molestiae, veritatis ex quis ratione eligendi quam adipisci 
        //      neque veniam culpa cumque vero reprehenderit rerum excepturi iusto aliquam voluptas aperiam. Libero odit 
        //      explicabo labore modi tempora numquam ipsa corrupti aliquid vitae, adipisci beatae eveniet? Natus eius 
        //      fugiat accusantium consequuntur exercitationem delectus atque, hic labore.   
        //     </p>
        //     <br/>
        //     <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore sequi, officiis unde aperiam 
        //         accusantium non dolorum debitis modi laboriosam maxime ullam at deleniti ipsa voluptate quasi! 
        //         Consectetur iusto deleniti, eveniet quos dolor ad nihil quidem assumenda, suscipit sequi amet? 
        //         Laudantium cum tenetur ad officiis quia beatae doloribus nulla doloremque molestias quod ducimus 
        //         necessitatibus sunt reiciendis delectus quas atque corrupti molestiae, dolor, provident excepturi. 
        //         Nesciunt repellendus dolorum tempora, officia laborum quasi.
        //     </p>
        //     <br/>
        // </Layout>
        
    
