import React from 'react';
import Link from 'next/link';
import fetch from 'isomorphic-fetch';

const HOST_NAME = process.env.HOST_NAME || 'http://jsonplaceholder.typicode.com/';

export default class extends React.Component {
    static async getInitialProps({ query }) {
        const { id } = query;
        const data = await fetch(`${HOST_NAME}posts/${id}`);
        const item = await data.json();
        return item;
    }
    render() {
        const { title, type, artist } = this.props;
        return (
            <section>
                <h1>{ title }</h1>
                <p>{ type }</p>
                <p>{ artist }</p>
                <Link href="/">Back to home</Link>
            </section>
        );
    }
}