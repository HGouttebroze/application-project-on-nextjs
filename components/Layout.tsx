import Head from "next/head"
import Link from "next/link"
import Image from "next/image"
import { lazy } from "react"

export default function Layout({ children, page }) {
    return (
        <div className="bg-blue-50 pt-5 text-center min-h-screen">
            <Head>
                <title>{page}</title>
                {/* add meta for SEO */}
            </Head>
            <header className="container-lg">
                <h1 className="text-5xl mb-2">MUSIC PARTITIONS & TABLATURES</h1>
                <div className="inligne-grid grid-cols-2 gap-x-10 p-4">
                    <Link href="/">
                        <button className="bg-blue-200 p-3 m-2 rounded-3xl hover:shadow-md border-2 border-blue-300">
                            Accueil
                        </button>
                    </Link>
                    <Link href="/tablatures">
                        <button className="bg-blue-200 p-3 m-2 rounded-3xl hover:shadow-md border-2 border-blue-300">
                            Tablatures
                        </button>
                    </Link>
                </div>
                <div>
                    <Image src="/notesTree.png" 
                    alt="header-pic" 
                    width="300" 
                    height="375"
                     quality={100} 
                     />
                </div>
            </header>
            <main className="pt-4 mx-20">{children}</main>
                 
            <footer className="p10">
                <Image src="/patitions1.jpg"
                  alt="footer-pic" 
                  width="1000" 
                  height="80" 
                  className="rounded-3xl object-cover"
                  quality={100} 
                  />
                  <ul className="pt-10 pb-4 flex justify-around">
                      <li>A propos</li>
                      <li>Qui sommes-nous</li>
                      <li>Contact</li>
                      <li>Copyright</li>
                      </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, 
                          ratione? Maxime corrupti reiciendis, iusto vitae perspiciatis unde 
                          reprehenderit adipisci repellat quo dicta similique minus ullam officiis 
                          commodi nobis et pariatur.
                    </p>
            </footer>
        </div>
    )
}